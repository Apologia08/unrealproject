// Fill out your copyright notice in the Description page of Project Settings.

#include "IslandPlayerState.h"


AIslandPlayerState::AIslandPlayerState(const FObjectInitializer& ObejectInitializer) :Super(ObejectInitializer)
{
	MaxHealth = 100.f;
	CurrentHealth = 100.f;
	DefaultDamage = 10.f;
}

void AIslandPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AIslandPlayerState, MaxHealth);
	DOREPLIFETIME(AIslandPlayerState, CurrentHealth);
	DOREPLIFETIME(AIslandPlayerState, DefaultDamage);
}

void AIslandPlayerState::AddHealth(float AmountOfHeal)
{
	CurrentHealth += AmountOfHeal;

	if (CurrentHealth > MaxHealth)
		CurrentHealth = MaxHealth;
}

float AIslandPlayerState::GetCurrentHealth() const
{
	return CurrentHealth;
}

void AIslandPlayerState::SetCurrentHealth(float NewCurrentHealth)
{
	CurrentHealth = NewCurrentHealth;
}

float AIslandPlayerState::GetMaxHealth() const
{
	return MaxHealth;
}

void AIslandPlayerState::SetMaxHealth(float NewMaxHealth)
{
	MaxHealth = NewMaxHealth;
}

float AIslandPlayerState::GetDefaultDamage() const
{
	return DefaultDamage;
}

void AIslandPlayerState::SetDefaultDamage(float NewDefaultDamage)
{
	DefaultDamage = NewDefaultDamage;
}