// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BreakableActor.h"
#include "ExplosionActor.generated.h"

/**
 *
 */
UCLASS()
class ISLAND_API AExplosionActor : public ABreakableActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AExplosionActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

protected:

	FTimerHandle TimerHandle;
	FTimerHandle ColorTimer;
	int8 TimerCount = -1;
	

	UPROPERTY(VisibleAnywhere)
		class USphereComponent* AttackRange;

	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<AActor>> AttackingClass;

	UPROPERTY(EditAnywhere)
		float DefaultDamage = 0.f;

	UPROPERTY(EditAnywhere)
		float DelayTime = 1.f;

	UFUNCTION()
		void Explosion();

	UFUNCTION()
		void SetMaterialRed();

	UFUNCTION()
		void SetMaterialColorReset();

};
