// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"
#include "Components/StaticMeshComponent.h"
#include "BreakableActor.generated.h"

UCLASS()
class ISLAND_API ABreakableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABreakableActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(EditAnywhere)
		float CurrentHealth = 100.f;

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
		UStaticMeshComponent *StaticMeshComponent;

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
		USkeletalMeshComponent *SkeletalMeshComponent;

	UPROPERTY(EditAnywhere)
		class UParticleSystem* HitParticle;

	UPROPERTY(EditAnywhere)
		class USoundBase* HitSound;

public:
	UFUNCTION()
		virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

};
