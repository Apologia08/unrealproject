// Fill out your copyright notice in the Description page of Project Settings.

#include "ExplosionActor.h"
#include "Components/SphereComponent.h"
#include "IslandMonsterCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Components/SkeletalMeshComponent.h"


AExplosionActor::AExplosionActor()
{
	AttackRange = CreateDefaultSubobject<USphereComponent>(TEXT("AttackRange"));
	AttackRange->SetupAttachment(RootComponent);
}

void AExplosionActor::BeginPlay()
{
	Super::BeginPlay();

	SkeletalMeshComponent->CreateAndSetMaterialInstanceDynamic(0);
}

void AExplosionActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	TArray<AActor*> OverlapActors;
	AttackRange->GetOverlappingActors(OverlapActors);

	for (int i = 0; i < OverlapActors.Num(); i++)
	{
		for (int j = 0; j < AttackingClass.Num(); j++)
		{
			if (OverlapActors[i]->GetClass()->IsChildOf(AttackingClass[j]))
			{
				FDamageEvent ThisEvent(UDamageType::StaticClass());
				OverlapActors[i]->TakeDamage(DefaultDamage, ThisEvent, GetWorld()->GetFirstPlayerController(), this);
			}
		}
	}
}

float AExplosionActor::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	AActor::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	if (Role == ROLE_Authority && DamageCauser != this && CurrentHealth > 0 && !TimerHandle.IsValid() )
	{
		CurrentHealth -= Damage;

		if (CurrentHealth <= 0)
		{
			FTimerManager& WorldTimerManager = GetWorldTimerManager();
			WorldTimerManager.SetTimer(TimerHandle, this, &AExplosionActor::Explosion, DelayTime);
			SetMaterialRed();
		}

		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, GetActorLocation());
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticle, Cast<AIslandCharacter>(DamageCauser)->GetAttackedLocation());
	}
	return Damage;
}

void AExplosionActor::Explosion()
{
	GetWorldTimerManager().ClearAllTimersForObject(this);
	Destroy();
}

void AExplosionActor::SetMaterialRed()
{
	FTimerManager& WorldTimerManager = GetWorldTimerManager();

	SkeletalMeshComponent->SetVectorParameterValueOnMaterials("Color", FVector(1.f, 0.f, 0.f));
	WorldTimerManager.SetTimer(ColorTimer, this, &AExplosionActor::SetMaterialColorReset, 0.1);
	DelayTime -= 0.1;
}

void AExplosionActor::SetMaterialColorReset()
{
	FTimerManager& WorldTimerManager = GetWorldTimerManager();
	
	SkeletalMeshComponent->SetVectorParameterValueOnMaterials("Color", FVector(1.f, 1.f, 1.f));

	float DeltaTime = DelayTime * FMath::Pow(1.1, --TimerCount);
	WorldTimerManager.SetTimer(ColorTimer, this, &AExplosionActor::SetMaterialRed, DelayTime - DeltaTime);
	DelayTime = DeltaTime;


}
