// Fill out your copyright notice in the Description page of Project Settings.

#include "IslandBossCharacter.h"
#include "Components/CapsuleComponent.h"
#include "IslandGameModeBase.h"
#include "Engine/World.h"
#include "Components/SphereComponent.h"


AIslandBossCharacter::AIslandBossCharacter()
{
	AttackSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("AttackSphereComponent"));
	AttackSphereComponent->SetupAttachment(GetMesh(), "spine_01");
	AttackSphereComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	AttackSphereComponent->SetCollisionObjectType(ECC_GameTraceChannel3);
}

void AIslandBossCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (AttackSphereComponent)
		AttackSphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AIslandBossCharacter::CheckAttackRange);
}

void AIslandBossCharacter::CheckAttackRange(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (Role == ROLE_Authority && OtherActor != this)
	{

		for (int i = 0; i < AttackedActor.Num(); i++)
		{
			if (AttackedActor[i] == OtherActor)
			{
				return;
			}
		}


		for (int i = 0; i < AttackingClass.Num(); i++)
		{
			if (AttackingClass[i]->IsChildOf(AIslandCharacter::StaticClass()))
			{
				if (OtherActor->GetClass()->IsChildOf(AttackingClass[i]) && OtherComp == Cast<AIslandCharacter>(OtherActor)->GetCapsuleComponent())
				{

					AIslandCharacter* AttackedCharacter = Cast<AIslandCharacter>(OtherActor);
					FDamageEvent ThisEvent(UDamageType::StaticClass());
					AttackLocation = OverlapComp->GetComponentLocation();
					AttackedCharacter->MyTakeDamage(DefaultDamage * (1.0 + CurrentAttackPattern) * FMath::RandRange(1.f - RandomDamageRange, 1.f + RandomDamageRange), ThisEvent, this->GetController(), this);

					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attack Character");
					AttackedActor.Add(OtherActor);
				}
			}
			else
			{
				if (OtherActor->GetClass()->IsChildOf(AttackingClass[i]))
				{
					AActor* AttackedActor = Cast<AActor>(OtherActor);
					FDamageEvent ThisEvent(UDamageType::StaticClass());
					AttackLocation = OverlapComp->GetComponentLocation();
					AttackedActor->TakeDamage(DefaultDamage * (1.0 + CurrentAttackPattern) * FMath::RandRange(1.f - RandomDamageRange, 1.f + RandomDamageRange), ThisEvent, this->GetController(), this);

					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attack Actor");
					AIslandMonsterCharacter::AttackedActor.Add(OtherActor);
				}
			}
		}
	}
}

float AIslandBossCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	AIslandCharacter::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	if (DamageCauser != this)
	{
		CurrentState = ECharacterState::HIT;

		CurrentHealth -= Damage;

		if (CurrentHealth <= 0.f)
		{
			CurrentState = ECharacterState::DEATH;
			GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			Cast<AIslandGameModeBase>(GetWorld()->GetAuthGameMode())->StageRestart();
		}
	}

	return Damage;
}

void AIslandBossCharacter::ChooseAttackPattern(int AttackPattern)
{
	CurrentAttackPattern = AttackPattern;
}

void AIslandBossCharacter::ResetAttackPattern()
{
	CurrentAttackPattern = -1;
}

void AIslandBossCharacter::WeaponCollisionOn()
{
	Super::WeaponCollisionOn();

	if (bIsRangeAttack)
		AttackSphereComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}
void AIslandBossCharacter::WeaponCollisionOff()
{
	if (bIsRangeAttack)
		AttackSphereComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	Super::WeaponCollisionOff();
}