// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "IslandGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class ISLAND_API AIslandGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
		TArray<class AIslandMonsterCharacter*> ZombieMonsters;

	UPROPERTY(BlueprintReadOnly)
		int32 ZombieDeathCount;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void StageRestart();


};
