// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SpawnItem.h"
#include "Scarecrow.generated.h"

/**
 * 
 */
UCLASS()
class ISLAND_API AScarecrow : public ASpawnItem
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AScarecrow();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
protected:

	TArray<UMeshComponent*> MeshArray;

	FTimerHandle TimerHandle;

	class UParticleSystemComponent* ParticleComponent;

	UPROPERTY(EditAnywhere)
		float CreateTime = 2.f;

	UPROPERTY(EditAnywhere)
		float DeathDelayTime = 2.f;

	UPROPERTY(EditAnywhere)
		UParticleSystem* SpawnParticle;
		

	float TimeCount = 0.f;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* SecondStaticMeshComponent;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* ThirdStaticMeshComponent;

	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* SkeletalMeshComponent;

	UPROPERTY(EditAnywhere)
		TArray<UMaterialInterface*> ChangeMaterial;

	void CallDestroy();

public:

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	virtual void SetCollision() override;

	UFUNCTION()
		void AfterSpawn();

	UFUNCTION()
		void SetMaterialRed();

	UFUNCTION()
		void SetMaterialColorReset();


};
