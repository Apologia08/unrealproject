// Fill out your copyright notice in the Description page of Project Settings.

#include "Scarecrow.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/SkeletalMesh.h"
#include "TimerManager.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

AScarecrow::AScarecrow()
{	
	PrimaryActorTick.bCanEverTick = false;

	SecondStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	SecondStaticMeshComponent->SetupAttachment(StaticMeshComponent);
	SecondStaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshArray.Add(SecondStaticMeshComponent);

	ThirdStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ThirdStaticMeshComponent"));
	ThirdStaticMeshComponent->SetupAttachment(StaticMeshComponent);
	ThirdStaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshArray.Add(ThirdStaticMeshComponent);

	SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMeshComponent->SetupAttachment(StaticMeshComponent);
	SkeletalMeshComponent->SetCollisionProfileName("SpawnItem");
	MeshArray.Add(SkeletalMeshComponent);

}

void AScarecrow::BeginPlay()
{
	Super::BeginPlay();
	
	for (int i = 0; i < MeshArray.Num(); i++)
	{
		MeshArray[i]->CreateAndSetMaterialInstanceDynamic(0);
		MeshArray[i]->SetScalarParameterValueOnMaterials("Opacity", 0.5);
	}
}

// Called every frame
void AScarecrow::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//TArray<AActor*> OtherActors;
	//GetOverlappingActors(OtherActors);
	//if (OtherActors.Num() > 0)
	//{
	//	SetMaterialRed();
	//}
	//else
	//{
	//	SetMaterialColorReset();
	//}
}

void AScarecrow::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);


		
}


float AScarecrow::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	AItem::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	if (DamageCauser != this && CurrentHealth > 0)
	{
		bIsHit = true;

		CurrentHealth -= Damage;

		if (CurrentHealth <= 0)
		{
			for (int i = 0; i < MeshArray.Num(); i++)
			{
				MeshArray[i]->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
				MeshArray[i]->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
				MeshArray[i]->SetSimulatePhysics(true);
			}

			StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

			if (ParticleComponent)
			{
				ParticleComponent->DestroyComponent();
			}

			GetWorldTimerManager().SetTimer(TimerHandle, this, &AScarecrow::CallDestroy, DeathDelayTime);
		}
	}

	return Damage;
}

void AScarecrow::CallDestroy()
{
	Destroy();
}

void AScarecrow::SetCollision()
{
	StaticMeshComponent->SetCollisionProfileName("AfterSpawn");
}

void AScarecrow::AfterSpawn()
{
	if (ChangeMaterial.Num() == 3)
	{
		for (int i = 0; i < MeshArray.Num(); i++)
		{
			MeshArray[i]->SetMaterial(0, ChangeMaterial[i]);
		}
	}
	
	
	ParticleComponent = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SpawnParticle, GetActorLocation());
}

void AScarecrow::SetMaterialRed()
{
	for (int i = 0; i < MeshArray.Num(); i++)
	{
		MeshArray[i]->SetVectorParameterValueOnMaterials("Color", FVector(1.f, 0.f, 0.f));
	}
}

void AScarecrow::SetMaterialColorReset()
{
	for (int i = 0; i < MeshArray.Num(); i++)
	{
		MeshArray[i]->SetVectorParameterValueOnMaterials("Color", FVector(1.f, 1.f, 1.f));
	}
}