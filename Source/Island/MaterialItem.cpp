// Fill out your copyright notice in the Description page of Project Settings.

#include "MaterialItem.h"
#include "SurvivorCharacter.h"
#include "SurvivorPlayerState.h"
#include "TimerManager.h"

AMaterialItem::AMaterialItem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SetReplicates(true);
	StaticMeshComponent->SetCollisionProfileName("PickupItem");
}

void AMaterialItem::BeginPlay()
{
	Super::BeginPlay();

}

void AMaterialItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//if (GetOwner() != nullptr)
	//	MoveToOwner(DeltaTime);
}



void AMaterialItem::Pickup(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Overlap Comp : " + OtherComp->GetName());
	ASurvivorCharacter* SurvivorCharacter = Cast<ASurvivorCharacter>(GetOwner());
	if (Role == ROLE_Authority && SurvivorCharacter != nullptr && Cast<UCapsuleComponent>(OtherComp) == SurvivorCharacter->GetCapsuleComponent())
	{
		ServerAddMaterialToState(SurvivorCharacter);
	}
}


bool AMaterialItem::ServerAddMaterialToState_Validate(ASurvivorCharacter* TargetSurvivor)
{
	return true;
}

void AMaterialItem::ServerAddMaterialToState_Implementation(ASurvivorCharacter* TargetSurvivor)
{
	ASurvivorPlayerState* TargetPlayerState = Cast<ASurvivorPlayerState>(TargetSurvivor->PlayerState);
	
	if (TargetPlayerState != nullptr && TargetPlayerState->NumMaterials.Num() > StaticCast<uint8>(ItemMaterial))
	{
		TargetPlayerState->NumMaterials[StaticCast<uint8>(ItemMaterial)]++;
		GetWorldTimerManager().ClearAllTimersForObject(this);
		MulticastDestroy();
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Num Material : " + FString::FromInt(TargetPlayerState->NumMaterials[StaticCast<uint8>(ItemMaterial)]));
	}
}

void AMaterialItem::MulticastDestroy_Implementation()
{
	Destroy();
}