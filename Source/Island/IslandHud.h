// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "IslandHud.generated.h"

/**
 * 
 */
UCLASS()
class ISLAND_API AIslandHud : public AHUD
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere)
		UFont* HUDFont;

	AIslandHud();

	virtual void DrawHUD() override;
	
	
};
