// Fill out your copyright notice in the Description page of Project Settings.

#include "SpawnItem.h"



// Sets default values
ASpawnItem::ASpawnItem()
{
	StaticMeshComponent->SetSimulatePhysics(false);
	StaticMeshComponent->SetCollisionProfileName("SpawnItem");
}

// Called when the game starts or when spawned
void ASpawnItem::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASpawnItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float ASpawnItem::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	if (DamageCauser != this && CurrentHealth > 0)
	{
		bIsHit = true;

		CurrentHealth -= Damage;

		if (CurrentHealth <= 0)
		{
			Destroy();
		}
	}

	return Damage;
}

TArray<FNumMaterial> ASpawnItem::GetRequiredMaterials() const
{
	return RequiredMaterials;
}

void ASpawnItem::SetCollision()
{
	StaticMeshComponent->SetCollisionProfileName("AfterSpawn");
}

void ASpawnItem::IncreaseHealth(float Health)
{
	MaxHealth += Health;
	CurrentHealth += Health;
}