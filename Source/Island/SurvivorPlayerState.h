// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IslandPlayerState.h"
#include "MaterialItem.h"
#include "SurvivorPlayerState.generated.h"

/**
 * 
 */

class APickupItem;


UCLASS()
class ISLAND_API ASurvivorPlayerState : public AIslandPlayerState
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(Replicated, EditAnywhere)
		float MaxStamina;

	UPROPERTY(Replicated, EditAnywhere)
		float CurrentStamina;

	UPROPERTY(Replicated, BlueprintReadWrite)
		TArray<int> NumMaterials;

	UPROPERTY(Replicated)
		int8 PlayerLevel;

	UPROPERTY(Replicated)
		int32 SkillPoints;

	UPROPERTY(Replicated)
		float CurrentXP;

	UPROPERTY(Replicated)
		float NeededXP;

	UPROPERTY(Replicated)
		TArray<APickupItem*> PossessedPickupItem;

	UPROPERTY()
		int8 CurrentEmptySlotNum;

	int8 CheckInventoryEmpty();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		TArray<APickupItem*> GetPossessedItem() const;

	UFUNCTION(BlueprintCallable, Category = "Stat")
		float GetMaxStamina() const;

	UFUNCTION(BlueprintCallable, Category = "Stat")
		void SetMaxStamina(float NewMaxStamina);

	UFUNCTION(BlueprintCallable, Category = "Stat")
		float GetCurrentStamina() const;

	UFUNCTION(BlueprintCallable, Category = "Stat")
		void SetCurrentStamina(float NewCurrentStamina);

	UFUNCTION(BlueprintCallable, Category = "Stat")
		int GetPlayerLevel() const;

	UFUNCTION(BlueprintCallable, Category = "Stat")
		void SetPlayerLevel(int NewPlayerLevel);

	UFUNCTION(BlueprintCallable, Category = "Stat")
		int GetSkillPoints() const;

	UFUNCTION(BlueprintCallable, Category = "Stat")
		void SetSkillPoints(int NewSkillPoints);

	UFUNCTION(BlueprintCallable, Category = "Stat")
		float GetCurrentXP() const;

	UFUNCTION(BlueprintCallable, Category = "Stat")
		void SetCurrentXP(float NewCurrentXP);

	UFUNCTION(BlueprintCallable, Category = "Stat")
		float GetNeededXP() const;

	UFUNCTION(BlueprintCallable, Category = "Stat")
		void SetNeededXP(float NewNeededXp);


};
