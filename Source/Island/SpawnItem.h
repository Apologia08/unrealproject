// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "SpawnItem.generated.h"

/**
 * 
 */
//Forward declaration
enum class EKindOfItemMaterial : uint8;

USTRUCT()
struct FNumMaterial
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
		EKindOfItemMaterial Material;

	UPROPERTY(EditAnywhere)
		int8 NumMaterial;
};

UCLASS()
class ISLAND_API ASpawnItem : public AItem
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ASpawnItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxHealth = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float CurrentHealth = 100.f;

	UPROPERTY(EditAnywhere)
		TArray<FNumMaterial> RequiredMaterials;

	UPROPERTY()
		bool bIsHit = false;

public:
	UFUNCTION()
		TArray<FNumMaterial> GetRequiredMaterials() const;

	UFUNCTION()
		virtual void SetCollision();

	UFUNCTION()
		void IncreaseHealth(float Health);
};
