// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IslandCharacter.h"
#include "IslandMonsterCharacter.generated.h"

/**
 *
 */
UCLASS()
class ISLAND_API AIslandMonsterCharacter : public AIslandCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AIslandMonsterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stat")
		float MaxHealth = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stat")
		float CurrentHealth = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stat")
		float DefaultDamage = 10.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stat")
		float Exp = 10.f;


	virtual void CheckAttackRange(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult) override;

public:

	UFUNCTION(BlueprintCallable)
		void Attack();

	UFUNCTION(BlueprintCallable)
		void RefreshWalkSpeed();

	UFUNCTION(BlueprintCallable)
		void RefreshCurrentDamage();

	UFUNCTION(BlueprintCallable)
		void ChangeMovementSetting();

	UFUNCTION(BlueprintCallable)
		void ResetMovementSetting();

};
