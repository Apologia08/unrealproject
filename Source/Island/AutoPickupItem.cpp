// Fill out your copyright notice in the Description page of Project Settings.

#include "AutoPickupItem.h"


void AAutoPickupItem::BeginPlay()
{
	Super::BeginPlay();

	StaticMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AAutoPickupItem::Pickup);

}

void AAutoPickupItem::Pickup(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	Destroy();
}


void AAutoPickupItem::MoveToOwner()
{
	FVector Direction = GetOwner()->GetActorLocation() - GetActorLocation();
	Direction.Normalize();
	StaticMeshComponent->AddForce(Direction * StaticMeshComponent->GetBodyInstance()->GetBodyMass() * 80000.f);
	StaticMeshComponent->AddTorque(FVector::UpVector * StaticMeshComponent->GetBodyInstance()->GetBodyMass() * 320000.f);
}