// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/PlayerState.h"
#include "UObject/ConstructorHelpers.h"
#include "IslandPlayerState.generated.h"

/**
 * 
 */
class ASpawnItem;

UCLASS()
class ISLAND_API AIslandPlayerState : public APlayerState
{

	//엔진과 직접 통신할 일 없이 코드상에서만 잠시 사용할 가벼운 플러그인을 만들때 사용, 생성자를 만들어야함
	GENERATED_UCLASS_BODY()

public:

	UPROPERTY(Replicated, EditAnywhere)
		float MaxHealth;

	UPROPERTY(Replicated, EditAnywhere)
	 	float CurrentHealth;

	UPROPERTY(Replicated, EditAnywhere)
		float DefaultDamage;

	UFUNCTION()
		void AddHealth(float AmountOfHeal);

	UFUNCTION(BlueprintCallable, Category = "Stat")
		float GetCurrentHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Stat")
		void SetCurrentHealth(float NewCurrentHealth);

	UFUNCTION(BlueprintCallable, Category = "Stat")
		float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Stat")
		void SetMaxHealth(float NewMaxHealth);

	UFUNCTION(BlueprintCallable, Category = "Stat")
		float GetDefaultDamage() const;

	UFUNCTION(BlueprintCallable, Category = "Stat")
		void SetDefaultDamage(float NewDefaultDamage);


};
