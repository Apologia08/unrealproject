// Fill out your copyright notice in the Description page of Project Settings.

#include "SurvivorPlayerState.h"
#include "MaterialItem.h"


ASurvivorPlayerState::ASurvivorPlayerState(const FObjectInitializer& ObejectInitializer) :Super(ObejectInitializer)
{
	CurrentStamina = 100.f;
	MaxStamina = 100;

	NumMaterials.Init(0, StaticCast<int>(EKindOfItemMaterial::MATERIALCOUNT));
	PossessedPickupItem.Init(nullptr, 6);
}

void ASurvivorPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ASurvivorPlayerState, CurrentStamina);
	DOREPLIFETIME(ASurvivorPlayerState, MaxStamina);
	DOREPLIFETIME(ASurvivorPlayerState, NumMaterials);
	DOREPLIFETIME(ASurvivorPlayerState, PossessedPickupItem);
	DOREPLIFETIME(ASurvivorPlayerState, PlayerLevel);
	DOREPLIFETIME(ASurvivorPlayerState, SkillPoints);
	DOREPLIFETIME(ASurvivorPlayerState, CurrentXP);
	DOREPLIFETIME(ASurvivorPlayerState, NeededXP);
}

int8 ASurvivorPlayerState::CheckInventoryEmpty()
{
	for (int i = 0; i < PossessedPickupItem.Num(); i++)
	{
		if (PossessedPickupItem[i] == nullptr)
		{
			CurrentEmptySlotNum = i;
			return i;
		}
			
	}

	CurrentEmptySlotNum = -1;
	return -1;
}

TArray<APickupItem*> ASurvivorPlayerState::GetPossessedItem() const
{
	return PossessedPickupItem;
}

float ASurvivorPlayerState::GetMaxStamina() const
{
	return MaxStamina;
}

void ASurvivorPlayerState::SetMaxStamina(float NewMaxStamina)
{
	MaxStamina = NewMaxStamina;
}

float ASurvivorPlayerState::GetCurrentStamina() const
{
	return CurrentStamina;
}

void ASurvivorPlayerState::SetCurrentStamina(float NewCurrentStamina)
{
	CurrentStamina = NewCurrentStamina;
}

int ASurvivorPlayerState::GetPlayerLevel() const
{
	return PlayerLevel;
}

void ASurvivorPlayerState::SetPlayerLevel(int NewPlayerLevel)
{
	PlayerLevel = NewPlayerLevel;
}

int ASurvivorPlayerState::GetSkillPoints() const
{
	return SkillPoints;
}

void ASurvivorPlayerState::SetSkillPoints(int NewSkillPoints)
{
	SkillPoints = NewSkillPoints;
}

float ASurvivorPlayerState::GetCurrentXP() const
{
	return CurrentXP;
}

void ASurvivorPlayerState::SetCurrentXP(float NewCurrentXP)
{
	CurrentXP = NewCurrentXP;
}

float ASurvivorPlayerState::GetNeededXP() const
{
	return NeededXP;
}

void ASurvivorPlayerState::SetNeededXP(float NewNeededXp)
{
	NeededXP = NewNeededXp;
}