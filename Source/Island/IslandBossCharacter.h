// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IslandMonsterCharacter.h"
#include "IslandBossCharacter.generated.h"

/**
 * 
 */
UCLASS()
class ISLAND_API AIslandBossCharacter : public AIslandMonsterCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AIslandBossCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
protected:
	UPROPERTY(BlueprintReadOnly)
		int CurrentAttackPattern = -1;

	UPROPERTY(VisibleAnywhere)
		class USphereComponent* AttackSphereComponent;

public:

	virtual void CheckAttackRange(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult) override;
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	virtual void WeaponCollisionOn() override;
	virtual void WeaponCollisionOff() override;

	UPROPERTY(BlueprintReadWrite)
		bool bIsRangeAttack = false;

	UFUNCTION(BlueprintCallable)
		void ChooseAttackPattern(int AttackPattern);

	UFUNCTION(BlueprintCallable)
		void ResetAttackPattern();
	
};
