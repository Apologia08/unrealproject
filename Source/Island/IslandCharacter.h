// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/BoxComponent.h"
#include "IslandCharacter.generated.h"

UENUM(BlueprintType)
enum class ECharacterState : uint8
{
	IDLE,
	HIT,
	DEATH
};

UCLASS()
class ISLAND_API AIslandCharacter : public ACharacter
{
	GENERATED_BODY()



public:
	// Sets default values for this character's properties
	AIslandCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintNativeEvent)
		void MyTakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);

protected:

	TArray<AActor*> AttackedActor;

	UPROPERTY(EditAnywhere, Category = "Damage")
		float RandomDamageRange = 0.f;

	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<AActor>> AttackingClass;

	UPROPERTY()
		bool bIsClickAttack = false;

	UPROPERTY()
		float CurrentDamage = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed")
		float WalkSpeed = 300;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed")
		float RunSpeed = 400;

	UPROPERTY(EditAnywhere, Category = "Hit")
		class UParticleSystem* HitParticle;

	UPROPERTY(EditAnywhere, Category = "Hit")
		class USoundBase* HitSound;


	UPROPERTY()
		ECharacterState CurrentState = ECharacterState::IDLE;

	UPROPERTY()
		FVector AttackLocation = FVector::ZeroVector;

	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* LeftWeaponComponent;

	UPROPERTY(VisibleAnywhere)
		UBoxComponent* LeftAttackBoxComponent;

	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* RightWeaponComponent;

	UPROPERTY(VisibleAnywhere)
		UBoxComponent* RightAttackBoxComponent;

	UPROPERTY()
		FVector LeftDefaultAtkRange;

	UPROPERTY()
		FVector RightDefaultAtkRange;

protected:

	UFUNCTION(Server, Reliable, WithValidation)
		virtual void ServerAttack();

	UFUNCTION(BlueprintCallable)
		virtual void CheckAttackRange(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);


public:
	UPROPERTY(BlueprintReadWrite)
		bool bIsLeftHand = false;

	UPROPERTY(BlueprintReadWrite)
		bool bIsRightHand = false;

	UFUNCTION(BlueprintCallable)
		void SetIsClickAttackFalse();

	UFUNCTION(BlueprintCallable)
		bool GetIsClickAttack() const;

	UFUNCTION(BlueprintCallable)
		ECharacterState GetCurrentState() const;

	UFUNCTION(BlueprintCallable)
		void SetCurrentState(ECharacterState State);

	UFUNCTION(BlueprintCallable)
		virtual void WeaponCollisionOn();

	UFUNCTION(BlueprintCallable)
		virtual void WeaponCollisionOff();

	UFUNCTION(BlueprintCallable)
		FVector GetWeaponComponentLocation() const;

	UFUNCTION(BlueprintCallable)
		FVector GetAttackedLocation() const;

	UFUNCTION(BlueprintCallable)
		float GetWalkSpeed() const;

	UFUNCTION(BlueprintCallable)
		float GetRunSpeed() const;
};
