// Fill out your copyright notice in the Description page of Project Settings.

#include "PickupItem.h"
#include "SurvivorCharacter.h"
#include "SurvivorPlayerState.h"
#include "TimerManager.h"


// Sets default values
APickupItem::APickupItem()
{
	SetReplicates(true);

	StaticMeshComponent->SetCollisionProfileName("PickupItem");

}

// Called when the game starts or when spawned
void APickupItem::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void APickupItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickupItem::MoveToOwner()
{
	if (ASurvivorCharacter* SurvivorCharacter = Cast<ASurvivorCharacter>(GetOwner()))
	{
		if (Cast<ASurvivorPlayerState>(SurvivorCharacter->PlayerState)->CurrentEmptySlotNum != -1)
		{
			Super::MoveToOwner();
		}
		else
		{
			SetOwner(nullptr);
			GetWorldTimerManager().ClearAllTimersForObject(this);
		}
	}

	
}

void APickupItem::Pickup(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Overlap Comp : " + OtherComp->GetName());
	ASurvivorCharacter* SurvivorCharacter = Cast<ASurvivorCharacter>(GetOwner());
	
	if (SurvivorCharacter != nullptr && Cast<UCapsuleComponent>(OtherComp) == SurvivorCharacter->GetCapsuleComponent())
	{
		ASurvivorPlayerState* SurvivorState = Cast<ASurvivorPlayerState>(SurvivorCharacter->PlayerState);
		if(SurvivorState->CheckInventoryEmpty()!=-1)
		{
				SetActorEnableCollision(false);
				
				SurvivorState->PossessedPickupItem[SurvivorState->CurrentEmptySlotNum] = this;
				SetMeshSimulatePhysic(false);
				AttachToComponent(SurvivorCharacter->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "spine_01");
				SetActorRelativeLocation(FVector::ZeroVector);
				SetActorHiddenInGame(true);
				GetWorldTimerManager().ClearAllTimersForObject(this);
		}
		else
		{
			GetWorldTimerManager().ClearAllTimersForObject(this);
		}
	}
}

void APickupItem::SetMeshSimulatePhysic(bool bSetPhysic)
{
	StaticMeshComponent->SetSimulatePhysics(bSetPhysic);
}

TArray<FItemEffect> APickupItem::GetItemEffectList() const
{
	return ItemEffectList;
}

float APickupItem::GetAmountOfHeal() const
{
	return AmountOfHeal;
}