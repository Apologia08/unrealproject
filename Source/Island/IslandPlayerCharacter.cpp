// Fill out your copyright notice in the Description page of Project Settings.

#include "IslandPlayerCharacter.h"
#include "BreakableActor.h"
#include "IslandPlayerController.h"
#include "SurvivorPlayerState.h"
#include "SpawnItem.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "IslandCharacter.h"

// Sets default values
AIslandPlayerCharacter::AIslandPlayerCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	GetMesh()->RelativeRotation = FRotator(0, -90.f, 0);

	//Create a camera boom
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 1000.f;
	CameraBoom->RelativeRotation = FRotator(-80.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm


	//// Create a text render
	//TextRenderComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRender"));
	//TextRenderComponent->SetupAttachment(RootComponent);
	//TextRenderComponent->SetRelativeLocation(FVector(0.f, 0.f, 90.f));
	//TextRenderComponent->SetText(FString::SanitizeFloat(100.f));

	// Create a pickup range
	PickupRangeComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("PickupRange"));
	PickupRangeComponent->SetupAttachment(RootComponent);
	PickupRangeComponent->SetRelativeLocation(FVector(30.f, 0.f, -50.f));
	PickupRangeComponent->SetCollisionObjectType(ECC_GameTraceChannel3);

}

// Called when the game starts or when spawned
void AIslandPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	PickupRangeComponent->OnComponentBeginOverlap.AddDynamic(this, &AIslandPlayerCharacter::AddPickupItem);
	PickupRangeComponent->OnComponentEndOverlap.AddDynamic(this, &AIslandPlayerCharacter::DeletePickupItem);

}

// Called every frame
void AIslandPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ChoosePickupItem();

}


// Called to bind functionality to input

void AIslandPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// "movement" 바인딩 구성
	PlayerInputComponent->BindAxis("MoveForward", this, &AIslandPlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AIslandPlayerCharacter::MoveRight);
	PlayerInputComponent->BindAxis("RotateCameraYAxis", this, &AIslandPlayerCharacter::RotateCameraYAxis);
	PlayerInputComponent->BindAxis("RotateCameraXAxis", this, &AIslandPlayerCharacter::RotateCameraXAxis);


	//액션 바인딩
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &AIslandPlayerCharacter::ServerAttack);

	//카메라 회전 바인딩
	PlayerInputComponent->BindAction("CameraRotation", IE_Pressed, this, &AIslandPlayerCharacter::CameraRotationStart);
	PlayerInputComponent->BindAction("CameraRotation", IE_Released, this, &AIslandPlayerCharacter::CameraRotationEnd);

	//줍기 바인딩
	PlayerInputComponent->BindAction("Pickup", IE_Pressed, this, &AIslandPlayerCharacter::Pickup);

	PlayerInputComponent->BindAction("Skill", IE_Pressed, this, &AIslandPlayerCharacter::ClickSkill);
}



void AIslandPlayerCharacter::MoveForward(float Value)
{
	if (CurrentState == ECharacterState::IDLE && Value != 0.0f)
	{
		// add movement in that direction
		FVector DirectionVector = TopDownCameraComponent->GetForwardVector();
		DirectionVector -= FVector(0.f, 0.f, DirectionVector.Z);
		DirectionVector.Normalize();
		AddMovementInput(DirectionVector, Value);
	}

}

void AIslandPlayerCharacter::MoveRight(float Value)
{
	if (CurrentState == ECharacterState::IDLE && Value != 0.0f)
	{
		// add movement in that direction
		FVector DirectionVector = TopDownCameraComponent->GetRightVector();
		DirectionVector -= FVector(0.f, 0.f, DirectionVector.Z);
		DirectionVector.Normalize();
		AddMovementInput(DirectionVector, Value);
	}
}

void AIslandPlayerCharacter::CameraRotationStart()
{
	bIsRotateCamera = true;
}

void AIslandPlayerCharacter::CameraRotationEnd()
{
	bIsRotateCamera = false;
}

void AIslandPlayerCharacter::RotateCameraXAxis(float Value)
{
	if (bIsRotateCamera)
	{
		APlayerController *pController = Cast<APlayerController>(GetController());

		FVector2D MousePosition;
		pController->GetMousePosition(MousePosition.X, MousePosition.Y);

		FVector2D ScreenPos = GEngine->GameViewport->Viewport->GetSizeXY();
		if (MousePosition.Y > ScreenPos.Y / 2)
			CameraBoom->AddWorldRotation(FRotator(0.f, -Value * CameraSpeed, 0.f));
		else
			CameraBoom->AddWorldRotation(FRotator(0.f, Value * CameraSpeed, 0.f));
	}
}

void AIslandPlayerCharacter::RotateCameraYAxis(float Value)
{
	if (bIsRotateCamera)
	{
		APlayerController *pController = Cast<APlayerController>(GetController());

		FVector2D MousePosition;
		pController->GetMousePosition(MousePosition.X, MousePosition.Y);

		FVector2D ScreenPos = GEngine->GameViewport->Viewport->GetSizeXY();
		if (MousePosition.X > ScreenPos.X / 2)
			CameraBoom->AddWorldRotation(FRotator(0.f, -Value * CameraSpeed, 0.f));
		else
			CameraBoom->AddWorldRotation(FRotator(0.f, Value * CameraSpeed, 0.f));
	}

}

//void AIslandPlayerCharacter::CheckAttackRange(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
//{
//	if (Role == ROLE_Authority && OtherActor != this)
//	{
//		for (int i = 0; i < AttackingClass.Num(); i++)
//		{
//			if (AttackingClass[i]->IsChildOf(AIslandCharacter::StaticClass()))
//			{
//				if (OtherActor->GetClass()->IsChildOf(AttackingClass[i]) && OtherComp == Cast<AIslandCharacter>(OtherActor)->GetCapsuleComponent())
//				{
//					AIslandCharacter* AttackedCharacter = Cast<AIslandCharacter>(OtherActor);
//					FDamageEvent ThisEvent(UDamageType::StaticClass());
//					AttackedCharacter->TakeDamage(IslandPlayerState->DefaultDamage, ThisEvent, this->GetController(), this);
//					//AttackedCharacter->MulticastSetTextRender(AttackedCharacter->IslandPlayerState->CurrentHealth);
//					//AttackedCharacter->ClientSetHPBar(AttackedCharacter->IslandPlayerState->CurrentHealth);
//					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attack Character");
//				}
//			}
//			else
//			{
//				if (OtherActor->GetClass()->IsChildOf(AttackingClass[i]))
//				{
//					AActor* AttackedActor = Cast<AActor>(OtherActor);
//					FDamageEvent ThisEvent(UDamageType::StaticClass());
//					AttackedActor->TakeDamage(IslandPlayerState->DefaultDamage, ThisEvent, this->GetController(), this);
//					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attack Actor");
//				}
//			}
//
//		}
//	}
//	//AttackBoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
//}

void AIslandPlayerCharacter::ClickSkill(FKey Key)
{
	for (int i = 0; i < SkillList.Num(); i++)
	{
		if (CurrentUseSkill == -1 && Key == SkillList[i].SkillButton && Cast<ASurvivorPlayerState>(PlayerState)->CurrentStamina > SkillList[i].RequiredStamina)
		{
			CurrentUseSkill = i;
		}
	}
}

bool AIslandPlayerCharacter::UseSkill(int32 SkillNum)
{
	FTimerManager& WorldTimerManager = GetWorldTimerManager();
	ASurvivorPlayerState* ThisSuvivorState = Cast<ASurvivorPlayerState>(PlayerState);
	if (ThisSuvivorState->CurrentStamina > SkillList[SkillNum].RequiredStamina && !WorldTimerManager.IsTimerActive(SkillList[SkillNum].TimerHandle))
	{
		CurrentDamage = SkillList[SkillNum].SkillDamage;

		LeftAttackBoxComponent->SetWorldScale3D(SkillList[SkillNum].SkillRange);
		RightAttackBoxComponent->SetWorldScale3D(SkillList[SkillNum].SkillRange);


		if (ThisSuvivorState)
			ThisSuvivorState->CurrentStamina -= SkillList[SkillNum].RequiredStamina;

		WorldTimerManager.SetTimer(SkillList[SkillNum].TimerHandle, this, &AIslandPlayerCharacter::ResetSkillTimer, SkillList[SkillNum].CoolDownTime);

		return true;
	}

	return false;
}

void AIslandPlayerCharacter::ResetSkillTimer()
{
	FTimerManager& WorldTimerManager = GetWorldTimerManager();

	for (int i = 0; i < SkillList.Num(); i++)
	{
		if (WorldTimerManager.IsTimerActive(SkillList[i].TimerHandle) && WorldTimerManager.GetTimerRemaining(SkillList[i].TimerHandle) <= 0.f)
			WorldTimerManager.ClearTimer(SkillList[i].TimerHandle);
	}
}

void AIslandPlayerCharacter::ResetSkillNum()
{
	CurrentUseSkill = -1;
}

int32 AIslandPlayerCharacter::GetSkillNum() const
{
	return CurrentUseSkill;
}

float AIslandPlayerCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	if (Role == ROLE_Authority && DamageCauser != this)
	{

		if (FMath::RandRange(0.f, 100.f) <= HitReactProbability)
		{
			CurrentState = ECharacterState::HIT;
		}

		IslandPlayerState->CurrentHealth -= Damage;
		ClientSetHPBar(IslandPlayerState->CurrentHealth);
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, GetActorLocation());

		if(Cast<AIslandCharacter>(DamageCauser))
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticle, Cast<AIslandCharacter>(DamageCauser)->GetAttackedLocation());

		if (IslandPlayerState->CurrentHealth <= 0.f)
		{
			CurrentState = ECharacterState::DEATH;
			GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			DisableInput(Cast<APlayerController>(Controller));
			//Controller->UnPossess();
		}
	}
	return Damage;
}

void AIslandPlayerCharacter::MulticastSetTextRender_Implementation(float CurrentHealth)
{
	//TextRenderComponent->SetText(FString::SanitizeFloat(CurrentHealth));
	if (Cast<AIslandPlayerController>(GetController()))
		Cast<AIslandPlayerController>(GetController())->RefreshHUDHPBar();
}

void AIslandPlayerCharacter::ClientSetHPBar_Implementation(float CurrentHealth)
{
	if (Cast<AIslandPlayerController>(GetController()))
		Cast<AIslandPlayerController>(GetController())->SetHUDHPBar(CurrentHealth);
}

void AIslandPlayerCharacter::AddPickupItem(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{

	if (OtherActor->GetClass()->IsChildOf(APickupItem::StaticClass()))
	{
		PickupItems.Add(OtherActor);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::FromInt(PickupItems.Num()));
	}
}

void AIslandPlayerCharacter::DeletePickupItem(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	int32 CheckIndex = PickupItems.Find(OtherActor);

	if (CheckIndex != INDEX_NONE)
	{
		PickupItems.Remove(OtherActor);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::FromInt(PickupItems.Num()));
	}
}

void AIslandPlayerCharacter::ChoosePickupItem()
{
	if (PickupItems.Num() > 0)
	{

		float MinDistance = FVector::Distance(PickupItems[0]->GetActorLocation(), GetActorLocation());


		float CheckDistance;

		for (int i = 0; i < PickupItems.Num(); i++)
		{
			CheckDistance = FVector::Distance(PickupItems[i]->GetActorLocation(), GetActorLocation());
			if (MinDistance >= CheckDistance)
			{
				MinDistance = CheckDistance;
				ChosenPickupItem = Cast<APickupItem>(PickupItems[i]);
			}
		}
	}
}

void AIslandPlayerCharacter::Pickup()
{
	if (ChosenPickupItem != nullptr&&PickupItems.Num() > 0)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Pickup");

		ServerPickup(ChosenPickupItem);
		ChosenPickupItem = nullptr;
	}
}

bool AIslandPlayerCharacter::ServerPickup_Validate(APickupItem* PickupItem)
{
	return true;
}

void AIslandPlayerCharacter::ServerPickup_Implementation(APickupItem* PickupItem)
{
	PickupItem->Destroy();
}


void AIslandPlayerCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	IslandPlayerState = Cast<AIslandPlayerState>(PlayerState);
	CurrentDamage = IslandPlayerState->DefaultDamage;

	if (Role == ROLE_Authority && IslandPlayerState != nullptr)
	{
		//		IslandPlayerState->CurrentHealth = 100.f;
	}
}


AIslandPlayerState* AIslandPlayerCharacter::GetIslandPlayerState()
{
	if (IslandPlayerState)
	{
		return IslandPlayerState;
	}
	else
	{
		IslandPlayerState = Cast<AIslandPlayerState>(PlayerState);
		return IslandPlayerState;
	}
}

