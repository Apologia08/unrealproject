// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AutoPickupItem.h"
#include "PickupItem.generated.h"

/**
 * 
 */

UENUM()
enum class EBuffKind : uint8
{
	STAMINA_REGEN,
	DEFAULT_DAMAGE,
	SPEED,
	ATK_SPEED,
	BUFF_KIND_COUNT UMETA(Hidden)
};

USTRUCT()
struct FItemEffect
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere)
		EBuffKind BuffKind;

	UPROPERTY(EditAnywhere)
		float BuffRate;

	UPROPERTY(EditAnywhere)
		float BuffTime;

};

UCLASS()
class ISLAND_API APickupItem : public AAutoPickupItem
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	APickupItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

protected:

	UPROPERTY(EditAnywhere)
		float AmountOfHeal;

	UPROPERTY(EditAnywhere)
		TArray<FItemEffect> ItemEffectList;

public:

	UPROPERTY(EditAnywhere)
		UParticleSystem* UseParticle;

	UPROPERTY(EditAnywhere)
		class USoundBase* UseSound;

protected:

	virtual void Pickup(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult) override;
	virtual void MoveToOwner() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetMeshSimulatePhysic(bool bSetPhysic);

	TArray<FItemEffect> GetItemEffectList() const;

	float GetAmountOfHeal() const;
	
};
