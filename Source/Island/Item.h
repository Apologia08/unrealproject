// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/Texture2D.h"
#include "Item.generated.h"

USTRUCT(BlueprintType)
struct FItemInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* ItemImage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText PickupText;
};

UCLASS()
class ISLAND_API AItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	bool bIsSurvivorItem;	

	UPROPERTY(EditAnywhere)
		FItemInfo ItemInfo;

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
		UStaticMeshComponent *StaticMeshComponent;

public:
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		FItemInfo GetIteminfo() const;
};
