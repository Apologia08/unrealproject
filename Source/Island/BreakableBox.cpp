// Fill out your copyright notice in the Description page of Project Settings.

#include "BreakableBox.h"
#include "Item.h"


ABreakableBox::ABreakableBox()
{

}

void ABreakableBox::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	ServerSpawnItem();
}

bool ABreakableBox::ServerSpawnItem_Validate()
{
	return true;
}

void ABreakableBox::ServerSpawnItem_Implementation()
{
	for (int i = 0; i < DropGroupList.Num(); i++)
	{
		SpawnFromDropItemList(DropGroupList[i]);
	}
}

void ABreakableBox::SpawnFromDropItemList(FDropGroup DropGroup)
{
	float TotalPossibility = 0.f;
	for (int i = 0; i < DropGroup.DropItemList.Num(); i++)
	{
		if (DropGroup.DropItemList[i].DropPossibilty >= 0)
			TotalPossibility += DropGroup.DropItemList[i].DropPossibilty;

		if (TotalPossibility > 100.f)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Above 100%");
			return;
		}
	}

	float RandomFloat = FMath::RandRange(0.f, 100.f);

	if (RandomFloat <= TotalPossibility)
	{
		for (int i = 0; i < DropGroup.DropItemList.Num(); i++)
		{

			if (RandomFloat > DropGroup.DropItemList[i].DropPossibilty)
			{
				RandomFloat -= DropGroup.DropItemList[i].DropPossibilty;
			}
			else
			{
				for (int j = 0; j < DropGroup.DropItemList[i].NumDrop; j++)
				{
					GetWorld()->SpawnActor<AItem>(DropGroup.DropItemList[i].Item, GetActorLocation(), GetActorRotation());
				}
				return;
			}
		}
	}
}