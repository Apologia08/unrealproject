// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IslandCharacter.h"
#include "UnrealNetwork.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/TextRenderComponent.h"

#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/DamageType.h"
#include "GameFramework/PlayerController.h"
#include "Components/StaticMeshComponent.h"
#include "IslandPlayerState.h"
#include "PickupItem.h"
#include "IslandPlayerCharacter.generated.h"

USTRUCT(BlueprintType)
struct FSkillInfo
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int SkillLevel = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SkillDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FKey SkillButton;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float RequiredStamina;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float CoolDownTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector SkillRange = FVector(1.f, 1.f, 1.f);
	   
	UPROPERTY(BlueprintReadWrite)
		FTimerHandle TimerHandle;

};


UCLASS()
class ISLAND_API AIslandPlayerCharacter : public AIslandCharacter
{
	GENERATED_BODY()



public:
	// Sets default values for this character's properties
	AIslandPlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

protected:
	TArray<AActor*> PickupItems;
	APickupItem* ChosenPickupItem;
	AIslandPlayerState* IslandPlayerState;

	bool bIsRotateCamera;

	UPROPERTY()
		int32 CurrentUseSkill = -1;

	UPROPERTY(EditAnywhere, Category = "Hit")
		float HitReactProbability = 100.f;

	UPROPERTY(EditAnywhere, Category = "Camera")
		float CameraSpeed = 5.f;

	UPROPERTY(VisibleAnywhere)
		UCameraComponent* TopDownCameraComponent;

	UPROPERTY(VisibleAnywhere)
		USpringArmComponent* CameraBoom;

	//UPROPERTY(VisibleAnywhere)
	//	UTextRenderComponent* TextRenderComponent;

	UPROPERTY(VisibleAnywhere)
		UBoxComponent* PickupRangeComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Skill")
		TArray<FSkillInfo> SkillList;

	UFUNCTION()
		virtual void ClickSkill(FKey Key);

	UFUNCTION(BlueprintCallable)
		bool UseSkill(int32 SkillNum);

	UFUNCTION()
		void ResetSkillTimer();

	UFUNCTION(BlueprintCallable)
		void ResetSkillNum();

	UFUNCTION(BlueprintCallable)
		int32 GetSkillNum() const;

	// 전후 이동 처리
	UFUNCTION()
		void MoveForward(float Value);

	// 좌우 이동 처리
	UFUNCTION()
		void MoveRight(float Value);

	//카메라 회전
	UFUNCTION()
		void CameraRotationStart();

	UFUNCTION()
		void CameraRotationEnd();

	UFUNCTION()
		void RotateCameraXAxis(float Value);

	UFUNCTION()
		void RotateCameraYAxis(float Value);

	//virtual void CheckAttackRange(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult) override;

	//multicast TextRenderComponent's text
	UFUNCTION(NetMulticast, Reliable)
		void MulticastSetTextRender(float CurrentHealth);

	UFUNCTION(Client, Reliable)
		void ClientSetHPBar(float CurrentHealth);

	UFUNCTION()
		virtual void PossessedBy(AController* NewController) override;

	//줍기
	UFUNCTION()
		void AddPickupItem(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION()
		void DeletePickupItem(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void ChoosePickupItem();

	UFUNCTION()
		void Pickup();

	UFUNCTION(Server, Reliable, WithValidation)
		virtual void ServerPickup(APickupItem* PickupItem);

public:
	AIslandPlayerState* GetIslandPlayerState();

};
