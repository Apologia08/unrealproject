// Fill out your copyright notice in the Description page of Project Settings.

#include "SurvivorCharacter.h"
#include "MaterialItem.h"
#include "SpawnItem.h"
#include "SurvivorPlayerState.h"
#include "BreakableActor.h"
#include "IslandPlayerController.h"
#include "IslandMonsterCharacter.h"
#include "TimerManager.h"
#include "Scarecrow.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "ExplosionActor.h"



ASurvivorCharacter::ASurvivorCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->bUseControllerDesiredRotation = true;

	PickupMaterialRange = CreateDefaultSubobject<USphereComponent>(TEXT("PickupSphereRange"));
	PickupMaterialRange->SetupAttachment(RootComponent);
	PickupMaterialRange->SetRelativeScale3D((FVector::OneVector) * 5.f);
	PickupMaterialRange->SetCollisionObjectType(ECC_GameTraceChannel3);

	//static ConstructorHelpers::FObjectFinder<UBlueprint> WoodBarricadeBP(TEXT("/Game/Blueprints/Items/SpawnItem/WoodBarricade.WoodBarricade"));
	//WoodBarricadeClass = WoodBarricadeBP.Object->GeneratedClass;

	//static ConstructorHelpers::FObjectFinder<UBlueprint> SteelBarricadeBP(TEXT("/Game/Blueprints/Items/SpawnItem/SteelBarricade.SteelBarricade"));
	//SteelBarricadeClass = SteelBarricadeBP.Object->GeneratedClass;

	BuffInfos.Init(FBuffInfo(), StaticCast<uint8>(EBuffKind::BUFF_KIND_COUNT));
	//BuffTimers.Init(FTimerHandle(), StaticCast<uint8>(EBuffKind::BUFF_KIND_COUNT));
	//BuffRates.Init(0.f, StaticCast<uint8>(EBuffKind::BUFF_KIND_COUNT));

}

void ASurvivorCharacter::BeginPlay()
{
	Super::BeginPlay();
	PickupMaterialRange->OnComponentBeginOverlap.AddDynamic(this, &ASurvivorCharacter::AutoPickup);

	if (SpawnItemList.Num() > 0)
	{
		TargetSpawnItemList = SpawnItemList[0];
		CurrentSpawnItemIndex = 0;
	}


}

void ASurvivorCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentState != ECharacterState::DEATH)
	{
		APlayerController* pController = Cast<APlayerController>(GetController());


		FVector ScreenMousePos;
		FVector2D ScreenCharacterPos;
		pController->GetMousePosition(ScreenMousePos.X, ScreenMousePos.Y);
		pController->ProjectWorldLocationToScreen(GetActorLocation(), ScreenCharacterPos);

		pController->SetControlRotation(FVector(ScreenCharacterPos.Y - ScreenMousePos.Y, ScreenMousePos.X - ScreenCharacterPos.X, 0.f).Rotation() + CameraBoom->GetComponentRotation());

		if (bIsGenerateMode && MouseSpawnItem != nullptr)
		{
			FHitResult HitRes;
			FVector MousePos;
			FVector MouseDir;
			FCollisionQueryParams ColQuery;

			//커서값받기
			pController->DeprojectMousePositionToWorld(MousePos, MouseDir);

			//월드위치로 변환
			GetWorld()->LineTraceSingleByChannel(HitRes, MousePos, MousePos + MouseDir * 100000.f, ECC_Visibility, ColQuery);

			FVector WorldPos = WorldToCoord(HitRes.Location, 100.f);
			WorldPos.Z = HitRes.Location.Z;
			CurrentMouseCoord = WorldPos;

			MouseSpawnItem->SetActorLocation(CurrentMouseCoord);
			MouseSpawnItem->SetActorRotation(FRotator(0.f, (CurrentMouseCoord - GetActorLocation()).Rotation().Yaw - 90.f, 0.f));

			TArray<AActor*> OverlapActors;
			MouseSpawnItem->GetOverlappingActors(OverlapActors);
			if (FVector::Distance(CurrentMouseCoord, GetActorLocation()) > SpawnRange || OverlapActors.Num() > 0)
			{
				Cast<AScarecrow>(MouseSpawnItem)->SetMaterialRed();
			}
			else
				Cast<AScarecrow>(MouseSpawnItem)->SetMaterialColorReset();
		}
	}

	if (!GetVelocity().IsZero() && bIsRunning && SurvivorPlayerState->CurrentStamina > 0)
	{
		SurvivorPlayerState->CurrentStamina -= DeltaTime * 10;
		GetCharacterMovement()->MaxWalkSpeed = RunSpeed * (1.f + BuffInfos[StaticCast<uint8>(EBuffKind::SPEED)].BuffRate);
	}
	else if (SurvivorPlayerState->CurrentStamina < SurvivorPlayerState->MaxStamina)
	{
		bIsRunning = false;
		SurvivorPlayerState->CurrentStamina += DeltaTime * 10 * (1.f + BuffInfos[StaticCast<uint8>(EBuffKind::STAMINA_REGEN)].BuffRate);
		GetCharacterMovement()->MaxWalkSpeed = WalkSpeed * (1.f + BuffInfos[StaticCast<uint8>(EBuffKind::SPEED)].BuffRate);
	}

}

void ASurvivorCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//생성모드 키 바인딩
	PlayerInputComponent->BindAction("SetGenerateMode", IE_Pressed, this, &ASurvivorCharacter::SetGenerateMode);
	PlayerInputComponent->BindAction("OffGenerateMode", IE_Released, this, &ASurvivorCharacter::OffGenerateMode);
	PlayerInputComponent->BindAction("SpawnItem", IE_Pressed, this, &ASurvivorCharacter::SpawnItem);

	//달리기 바인딩
	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &ASurvivorCharacter::Run);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &ASurvivorCharacter::Walk);

	PlayerInputComponent->BindAction("UseItem", IE_Pressed, this, &ASurvivorCharacter::UseItem);
	PlayerInputComponent->BindAction("ChooseSpawnItem", IE_Pressed, this, &ASurvivorCharacter::ChooseCurrentSpawnIndex);
	
}

void ASurvivorCharacter::Run()
{
	bIsRunning = true;
}

void ASurvivorCharacter::Walk()
{
	bIsRunning = false;
}

void ASurvivorCharacter::CheckAttackRange(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (Role == ROLE_Authority && OtherActor != this)
	{

		for (int i = 0; i < AttackedActor.Num(); i++)
		{
			if (AttackedActor[i] == OtherActor)
			{
				return;
			}
		}

		for (int i = 0; i < AttackingClass.Num(); i++)
		{
			if (AttackingClass[i]->IsChildOf(AIslandCharacter::StaticClass()))
			{
				if (OtherActor->GetClass()->IsChildOf(AttackingClass[i]) && OtherComp == Cast<AIslandCharacter>(OtherActor)->GetCapsuleComponent())
				{

					AIslandCharacter* AttackedCharacter = Cast<AIslandCharacter>(OtherActor);
					FDamageEvent ThisEvent(UDamageType::StaticClass());
					AttackLocation = OverlapComp->GetComponentLocation();
					AttackedCharacter->MyTakeDamage(CurrentDamage * FMath::RandRange(1.f - RandomDamageRange, 1.f + RandomDamageRange), ThisEvent, this->GetController(), this);

					ComboCount++;
					GetWorldTimerManager().SetTimer(ComboTimer, this, &ASurvivorCharacter::ResetCombo, ComboResetTime);

					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attack Character");
					AttackedActor.Add(OtherActor);
					//GetMesh()->GlobalAnimRateScale = 0.f;
					//GetWorldTimerManager().SetTimer(AnimationTimer, this, &ASurvivorCharacter::ResetAnimationTimer, 0.1);

				}
			}

			else if (AttackingClass[i]->IsChildOf(AExplosionActor::StaticClass()) )
			{
				if (OtherActor->GetClass()->IsChildOf(AttackingClass[i]) && OtherComp == Cast<AExplosionActor>(OtherActor)->GetRootComponent())
				{
					AActor* AttackedActor = Cast<AActor>(OtherActor);
					FDamageEvent ThisEvent(UDamageType::StaticClass());
					AttackLocation = OverlapComp->GetComponentLocation();
					AttackedActor->TakeDamage(CurrentDamage * FMath::RandRange(1.f - RandomDamageRange, 1.f + RandomDamageRange), ThisEvent, this->GetController(), this);

					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attack Actor");
					ASurvivorCharacter::AttackedActor.Add(OtherActor);
				}
			}

			else
			{
				if (OtherActor->GetClass()->IsChildOf(AttackingClass[i]))
				{
					AActor* AttackedActor = Cast<AActor>(OtherActor);
					FDamageEvent ThisEvent(UDamageType::StaticClass());
					AttackLocation = OverlapComp->GetComponentLocation();
					AttackedActor->TakeDamage(CurrentDamage * FMath::RandRange(1.f - RandomDamageRange, 1.f + RandomDamageRange), ThisEvent, this->GetController(), this);

					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attack Actor");
					ASurvivorCharacter::AttackedActor.Add(OtherActor);
				}
			}
		}
	}
}

void ASurvivorCharacter::ResetAnimationTimer()
{
	GetMesh()->GlobalAnimRateScale = 1.f;
	GetWorldTimerManager().ClearTimer(AnimationTimer);
}

void ASurvivorCharacter::ServerAttack_Implementation()
{
	if (!bIsGenerateMode)
	{
		Super::ServerAttack_Implementation();
		CurrentDamage = SurvivorPlayerState->DefaultDamage * (1.f + BuffInfos[StaticCast<uint8>(EBuffKind::DEFAULT_DAMAGE)].BuffRate);
	}
}

void ASurvivorCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	SurvivorPlayerState = Cast<ASurvivorPlayerState>(PlayerState);

	if (Role == ROLE_Authority && SurvivorPlayerState != nullptr)
	{
		//		IslandPlayerState->CurrentHealth = 100.f;
	}
}

void ASurvivorCharacter::ClickSkill(FKey Key)
{
	if (!bIsGenerateMode)
	{
		Super::ClickSkill(Key);
	}
}

//생성모드 끄고 켜기
void ASurvivorCharacter::SetGenerateMode(FKey Key)
{
	if (!bIsGenerateMode)
	{
		for (int i = 0; i < SpawnItemList.Num(); i++)
		{
			if (SpawnItemList[i].Key == Key && TargetSpawnItemList.RandomSpawnItemList.SpawnItemClass.Num() > 0)
			{
				bIsGenerateMode = !bIsGenerateMode;
				TargetSpawnItemList = SpawnItemList[i];
				CurrentSpawnItemIndex = i;
				MouseSpawnItem = GenerateMeshAtMouse(TargetSpawnItemList.RandomSpawnItemList.SpawnItemClass[FMath::RandRange(0, TargetSpawnItemList.RandomSpawnItemList.SpawnItemClass.Num() - 1)], CurrentMouseCoord);
			}
		}
	}
	else if (bIsGenerateMode && SpawnItemList[CurrentSpawnItemIndex].Key != Key)
	{
		ChooseCurrentSpawnIndex(Key);
	}
	else
	{
		bIsGenerateMode = !bIsGenerateMode;
		MouseSpawnItem->Destroy();
		MouseSpawnItem = nullptr;
	}

}

void ASurvivorCharacter::OffGenerateMode()
{
	if (bIsGenerateMode)
	{
		bIsGenerateMode = !bIsGenerateMode;
		MouseSpawnItem->Destroy();
		MouseSpawnItem = nullptr;
	}
}

void ASurvivorCharacter::SpawnItem()
{
	if (bIsGenerateMode)
	{
		TArray<AActor*> OtherActors;
		MouseSpawnItem->GetOverlappingActors(OtherActors);
		AIslandPlayerState* TargetPlayerState = Cast<AIslandPlayerState>(PlayerState);

		if (OtherActors.Num() == 0 && FVector::Distance(CurrentMouseCoord, GetActorLocation()) <= SpawnRange)
		{
			ServerSpawnItem(MouseSpawnItem->GetClass(), CurrentMouseCoord);
		}
	}
}

bool ASurvivorCharacter::ServerSpawnItem_Validate(TSubclassOf<ASpawnItem> ChosenItem, FVector MouseCoord)
{
	return true;
}

//서버에서 스폰
void ASurvivorCharacter::ServerSpawnItem_Implementation(TSubclassOf<ASpawnItem> ChosenItem, FVector MouseCoord)
{
	//마우스 위치에 메쉬생성

	ASpawnItem* SpawnItem = Cast<ASpawnItem>(ChosenItem->ClassDefaultObject);
	FTimerManager& WorldTimerManager = GetWorldTimerManager();

	if (SpawnItem != nullptr && CheckEnoughNumMaterial(SpawnItem) && !WorldTimerManager.IsTimerActive(SpawnItemList[CurrentSpawnItemIndex].TimerHandle))
	{
		SubNumMaterial(SpawnItem);
		ASpawnItem* SpawnItem = GenerateMeshAtMouse(ChosenItem, MouseCoord);
		SpawnItem->SetReplicates(true);
		SpawnItem->SetCollision();
		SpawnItem->IncreaseHealth(SpawnItemList[CurrentSpawnItemIndex].PlusHealth);

		if (AScarecrow* SpawnScarecrow = Cast<AScarecrow>(SpawnItem))
		{
			SpawnScarecrow->AfterSpawn();
		}

		WorldTimerManager.SetTimer(SpawnItemList[CurrentSpawnItemIndex].TimerHandle, this, &ASurvivorCharacter::ResetSkillTimer, SpawnItemList[CurrentSpawnItemIndex].CoolDownTime);
	}
}

void ASurvivorCharacter::ResetSpawnTimer()
{
	FTimerManager& WorldTimerManager = GetWorldTimerManager();

	for (int i = 0; i < SpawnItemList.Num(); i++)
	{
		if (WorldTimerManager.IsTimerActive(SpawnItemList[i].TimerHandle) && WorldTimerManager.GetTimerRemaining(SpawnItemList[i].TimerHandle) <= 0.f)
			WorldTimerManager.ClearTimer(SpawnItemList[i].TimerHandle);
	}
}

void ASurvivorCharacter::MulticastSpawnItem_Implementation(TSubclassOf<ASpawnItem> ChosenItem, FVector MouseCoord)
{
	ASpawnItem* SpawnItem = GenerateMeshAtMouse(ChosenItem, MouseCoord);
	SpawnItem->SetReplicates(true);
}

void ASurvivorCharacter::ChooseCurrentSpawnIndex(FKey Key)
{
	if (bIsGenerateMode)
	{
		for (int i = 0; i < SpawnItemList.Num(); i++)
		{
			if (Key == SpawnItemList[i].Key /*&& i < SpawnItemList.Num()*/)
			{
				if (SpawnItemList[i].RandomSpawnItemList.SpawnItemClass.Num() > 0)
				{
					TargetSpawnItemList = SpawnItemList[i];
					CurrentSpawnItemIndex = i;
				}


			}
		}

		if (MouseSpawnItem != nullptr)
		{
			MouseSpawnItem->Destroy();
			MouseSpawnItem = GenerateMeshAtMouse(TargetSpawnItemList.RandomSpawnItemList.SpawnItemClass[FMath::RandRange(0, TargetSpawnItemList.RandomSpawnItemList.SpawnItemClass.Num() - 1)], CurrentMouseCoord);
		}
	}
}

//마우스 위치에 메쉬생성
ASpawnItem* ASurvivorCharacter::GenerateMeshAtMouse(TSubclassOf<ASpawnItem> ChosenItem, FVector MouseCoord)
{
	ASpawnItem* SpawnItem = GetWorld()->SpawnActor<ASpawnItem>(ChosenItem, MouseCoord, FRotator(0.f, (MouseCoord - GetActorLocation()).Rotation().Yaw - 90.f, 0.f));
	return SpawnItem;
}

FVector ASurvivorCharacter::WorldToCoord(FVector WorldLocation, int32 CoordRange)
{
	FVector Coord(WorldLocation.X, WorldLocation.Y, 0.f);

	int32 X = Coord.X;
	int32 Y = Coord.Y;

	if (X > 0)
		X = (X / CoordRange) * CoordRange + CoordRange / 2;
	else
		X = (X / CoordRange) * CoordRange - CoordRange / 2;

	if (Y > 0)
		Y = (Y / CoordRange) * CoordRange + CoordRange / 2;
	else
		Y = (Y / CoordRange) * CoordRange - CoordRange / 2;

	Coord.X = X;
	Coord.Y = Y;
	return Coord;
}

bool ASurvivorCharacter::CheckEnoughNumMaterial(ASpawnItem* TargetItem)
{
	if (Role == ROLE_Authority)
	{
		ASurvivorPlayerState* TargetPlayerState = Cast<ASurvivorPlayerState>(PlayerState);
		TArray<FNumMaterial> NumMaterials = TargetItem->GetRequiredMaterials();

		for (int8 i = 0; i < NumMaterials.Num(); i++)
		{
			if (NumMaterials[i].NumMaterial > TargetPlayerState->NumMaterials[StaticCast<uint8>(NumMaterials[i].Material)])
			{
				return false;
			}
		}
		return true;
	}
	return false;
}

void ASurvivorCharacter::SubNumMaterial(ASpawnItem* TargetItem)
{
	if (Role == ROLE_Authority)
	{
		ASurvivorPlayerState* TargetPlayerState = Cast<ASurvivorPlayerState>(PlayerState);
		TArray<FNumMaterial> NumMaterials = TargetItem->GetRequiredMaterials();
		for (int8 i = 0; i < NumMaterials.Num(); i++)
		{
			TargetPlayerState->NumMaterials[StaticCast<uint8>(NumMaterials[i].Material)] -= NumMaterials[i].NumMaterial;
		}
	}
}

//FVector ASurvivorCharacter::MoveSpawnItem(AActor* MovedActor)
//{
//		FHitResult HitRes;
//		FVector MousePos;
//		FVector MouseDir;
//		APlayerController *pController = Cast<APlayerController>(GetController());
//		FCollisionQueryParams ColQuery;
//
//		//커서값받기
//		pController->DeprojectMousePositionToWorld(MousePos, MouseDir);
//
//		//월드위치로 변환
//		GetWorld()->LineTraceSingleByChannel(HitRes, MousePos, MousePos + MouseDir * 100000.f, ECC_Visibility, ColQuery);
//
//		//좌표값으로 변환
//		FVector WorldPos = WorldToCoord(HitRes.Location, 100.f);
//		WorldPos.Z = HitRes.Location.Z;
//
//		//좌표로 이동
//		MovedActor->SetActorLocation(WorldPos);
//		return WorldPos;
//
//}

void ASurvivorCharacter::AutoPickup(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (OtherActor->GetClass()->IsChildOf(AAutoPickupItem::StaticClass()) && OtherActor->GetOwner() == nullptr)
	{
		AAutoPickupItem* AutoPickupItem = Cast<AAutoPickupItem>(OtherActor);
		
		if (Cast<APickupItem>(AutoPickupItem))
		{
			if (SurvivorPlayerState->CheckInventoryEmpty() != -1)
			{
				AutoPickupItem->SetOwner(this);
				FTimerHandle Timer;
				GetWorldTimerManager().SetTimer(Timer, AutoPickupItem, &AAutoPickupItem::MoveToOwner, 0.1, true);
			}
		}
		else
		{
			AutoPickupItem->SetOwner(this);
			FTimerHandle Timer;
			GetWorldTimerManager().SetTimer(Timer, AutoPickupItem, &AAutoPickupItem::MoveToOwner, 0.1, true);
		}


		
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Set Owner");
	}
}

void ASurvivorCharacter::Pickup(int8 InventorySlotNumber, APickupItem* PickupItem)
{
	SurvivorPlayerState->PossessedPickupItem[InventorySlotNumber] = PickupItem;
	MulticastAttach(PickupItem);
	PickupItem->SetOwner(this);
	PickupItem->SetActorHiddenInGame(true);
}

bool ASurvivorCharacter::ServerPickup_Validate(APickupItem* PickupItem)
{
	return true;
}

void ASurvivorCharacter::ServerPickup_Implementation(APickupItem* PickupItem)
{
	ASurvivorPlayerState* SurvivorPlayerState = Cast<ASurvivorPlayerState>(PlayerState);

	TArray<APickupItem*>& InventorySlot = SurvivorPlayerState->PossessedPickupItem;
	for (int i = 0; i < InventorySlot.Num(); i++)
	{
		if (InventorySlot[i] == nullptr)
		{
			SurvivorPlayerState->PossessedPickupItem[i] = PickupItem;
			//ClientSetInventorySlot(SurvivorPlayerState->PossessedPickupItem.Num() - 1, PickupItem->GetIteminfo().ItemImage);
			MulticastAttach(PickupItem);
			PickupItem->SetOwner(this);
			PickupItem->SetActorHiddenInGame(true);

			//if (PickupItem->GetActorEnableCollision())
			//{
			//	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Collision True");
			//}
			//else
			//{
			//	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Collision False");
			//}

			//if (PickupItem->IsAttachedTo(this))
			//{
			//	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attached");
			//}
			//else
			//	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "PickupItemName : " + PickupItem->GetName());

			break;
		}
	}
}

void ASurvivorCharacter::MulticastAttach_Implementation(APickupItem* PickupItem)
{
	if (PickupItem->GetOwner() == nullptr)
	{
		PickupItem->SetMeshSimulatePhysic(false);
		PickupItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "spine_01");
		PickupItem->SetActorRelativeLocation(FVector::ZeroVector);
		PickupItem->SetActorEnableCollision(false);
	}
}

void ASurvivorCharacter::ClientSetInventorySlot_Implementation(int32 NumSlot, UTexture2D* ItemImage)
{
	if (Cast<AIslandPlayerController>(GetController()))
		Cast<AIslandPlayerController>(GetController())->SetInventorySlot(NumSlot, ItemImage);
}

void ASurvivorCharacter::UseItem(FKey Key)
{
	int8 NumInventory;

	if (Key == EKeys::One)
	{
		NumInventory = 0;
	}

	else if (Key == EKeys::Two)
	{
		NumInventory = 1;
	}

	else if (Key == EKeys::Three)
	{
		NumInventory = 2;
	}

	else if (Key == EKeys::Four)
	{
		NumInventory = 3;
	}

	else if (Key == EKeys::Five)
	{
		NumInventory = 4;
	}

	else if (Key == EKeys::Six)
	{
		NumInventory = 5;
	}

	APickupItem* ChosenItem = SurvivorPlayerState->PossessedPickupItem[NumInventory];

	if (ChosenItem)
	{
		TArray<FItemEffect> ItemEffectList = SurvivorPlayerState->PossessedPickupItem[NumInventory]->GetItemEffectList();

		SurvivorPlayerState->AddHealth(ChosenItem->GetAmountOfHeal());

		for (int i = 0; i < ItemEffectList.Num(); i++)
		{
			int8 BuffKind = StaticCast<int8>(ItemEffectList[i].BuffKind);

			BuffInfos[BuffKind].BuffRate = ItemEffectList[i].BuffRate;

			if (BuffInfos[BuffKind].BuffEffect == nullptr)
			{
				BuffInfos[BuffKind].BuffEffect = UGameplayStatics::SpawnEmitterAttached(ChosenItem->UseParticle, GetMesh());
				BuffInfos[BuffKind].BuffEffect->SetRelativeScale3D(FVector::OneVector / 3);
			}


			GetWorldTimerManager().SetTimer(BuffInfos[BuffKind].TimerHandle, this, &ASurvivorCharacter::ResetBuff, ItemEffectList[i].BuffTime);
			GetCharacterMovement()->MaxWalkSpeed = WalkSpeed * (1.f + BuffInfos[StaticCast<uint8>(EBuffKind::SPEED)].BuffRate);


		}

		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ChosenItem->UseSound, GetActorLocation());
		//BuffEffect = UGameplayStatics::SpawnEmitterAttached(ChosenItem->UseParticle, GetMesh());

		//UGameplayStatics::SpawnEmitterAttached(ChosenItem->UseParticle, GetMesh());

		ChosenItem->Destroy();
		SurvivorPlayerState->PossessedPickupItem[NumInventory] = nullptr;


	}


}

void ASurvivorCharacter::ResetBuff()
{
	FTimerManager& WorldTimerManager = GetWorldTimerManager();
	for (int i = 0; i < BuffInfos.Num(); i++)
	{
		if (WorldTimerManager.IsTimerActive(BuffInfos[i].TimerHandle) && WorldTimerManager.GetTimerRemaining(BuffInfos[i].TimerHandle) <= 0.f)
		{
			BuffInfos[i].BuffRate = 0.f;
			GetCharacterMovement()->MaxWalkSpeed = WalkSpeed * (1.f + BuffInfos[StaticCast<uint8>(EBuffKind::SPEED)].BuffRate);
			WorldTimerManager.ClearTimer(BuffInfos[i].TimerHandle);
			BuffInfos[i].BuffEffect->DestroyComponent();
			BuffInfos[i].BuffEffect = nullptr;
			//BuffEffect->DestroyComponent();
		}
	}
}

float ASurvivorCharacter::GetBuffAtkSpeed()
{
	return BuffInfos[StaticCast<int8>(EBuffKind::ATK_SPEED)].BuffRate;
}

void ASurvivorCharacter::ResetCombo()
{
	ComboCount = 0;
}