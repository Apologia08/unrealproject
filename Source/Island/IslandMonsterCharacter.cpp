// Fill out your copyright notice in the Description page of Project Settings.

#include "IslandMonsterCharacter.h"
#include "Components/BoxComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BreakableActor.h"
#include "Components/CapsuleComponent.h"
#include "SpawnItem.h"
#include "Engine/World.h"
#include "IslandGameModeBase.h"
#include "SurvivorCharacter.h"
#include "Kismet/GameplayStatics.h"


AIslandMonsterCharacter::AIslandMonsterCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->bUseRVOAvoidance = true;
	GetCharacterMovement()->SetRVOAvoidanceWeight(0.5f);
	GetCharacterMovement()->AvoidanceConsiderationRadius = 300.f;

}

// Called when the game starts or when spawned
void AIslandMonsterCharacter::BeginPlay()
{
	Super::BeginPlay();
	CurrentDamage = DefaultDamage;
}

// Called every frame
void AIslandMonsterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


float AIslandMonsterCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	if (DamageCauser != this)
	{
		CurrentState = ECharacterState::HIT;

		CurrentHealth -= Damage;


		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, GetActorLocation());
		if (Cast<AIslandCharacter>(DamageCauser))
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticle, Cast<AIslandCharacter>(DamageCauser)->GetAttackedLocation());

		if (CurrentHealth <= 0.f)
		{
			CurrentState = ECharacterState::DEATH;
			//GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			Cast<AIslandGameModeBase>(GetWorld()->GetAuthGameMode())->ZombieMonsters.Remove(this);

			if (GetWorld()->GetFirstPlayerController())
				Cast<ASurvivorCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter())->AddExp(Exp);

			Cast<AIslandGameModeBase>(GetWorld()->GetAuthGameMode())->ZombieDeathCount++;
		}
	}

	return Damage;
}

void AIslandMonsterCharacter::CheckAttackRange(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (Role == ROLE_Authority && OtherActor != this)
	{

		for (int i = 0; i < AttackedActor.Num(); i++)
		{
			if (AttackedActor[i] == OtherActor)
			{
				return;
			}
		}


		for (int i = 0; i < AttackingClass.Num(); i++)
		{
			if (AttackingClass[i]->IsChildOf(AIslandCharacter::StaticClass()))
			{
				if (OtherActor->GetClass()->IsChildOf(AttackingClass[i]) && OtherComp == Cast<AIslandCharacter>(OtherActor)->GetCapsuleComponent())
				{

					AIslandCharacter* AttackedCharacter = Cast<AIslandCharacter>(OtherActor);
					FDamageEvent ThisEvent(UDamageType::StaticClass());
					AttackLocation = OverlapComp->GetComponentLocation();
					AttackedCharacter->MyTakeDamage(DefaultDamage * FMath::RandRange(1.f - RandomDamageRange, 1.f + RandomDamageRange), ThisEvent, this->GetController(), this);

					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attack Character");
					AttackedActor.Add(OtherActor);
				}
			}
			else
			{
				if (OtherActor->GetClass()->IsChildOf(AttackingClass[i]))
				{
					AActor* AttackedActor = Cast<AActor>(OtherActor);
					FDamageEvent ThisEvent(UDamageType::StaticClass());
					AttackLocation = OverlapComp->GetComponentLocation();
					AttackedActor->TakeDamage(DefaultDamage * FMath::RandRange(1.f - RandomDamageRange, 1.f + RandomDamageRange), ThisEvent, this->GetController(), this);

					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attack Actor");
					AIslandMonsterCharacter::AttackedActor.Add(OtherActor);
				}
			}
		}
	}
	//AttackBoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
}

void AIslandMonsterCharacter::Attack()
{
	bIsClickAttack = true;
}

void AIslandMonsterCharacter::RefreshWalkSpeed()
{
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
}

void AIslandMonsterCharacter::RefreshCurrentDamage()
{
	CurrentDamage = DefaultDamage;
}

void AIslandMonsterCharacter::ChangeMovementSetting()
{
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->bUseControllerDesiredRotation = true;
}

void AIslandMonsterCharacter::ResetMovementSetting()
{
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->bUseControllerDesiredRotation = false;
}