// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "MonsterAIController.generated.h"

/**
 * 
 */
UCLASS()
class ISLAND_API AMonsterAIController : public AAIController
{
	GENERATED_BODY()
	
public:

	AMonsterAIController(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//virtual void Possess(APawn* InPawn) override;

protected:
	
	UPROPERTY(EditAnywhere)
		class UBlackboardData* BlackboardAsset;

	UPROPERTY(EditAnywhere)
		class UBehaviorTree* BehaviorTressAsset;
};
