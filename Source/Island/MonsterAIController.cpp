// Fill out your copyright notice in the Description page of Project Settings.

#include "MonsterAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Navigation/CrowdFollowingComponent.h"

AMonsterAIController::AMonsterAIController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UCrowdFollowingComponent>(TEXT("PathFollowingComponent")))
{

}

void AMonsterAIController::BeginPlay()
{
	Super::BeginPlay();

	UseBlackboard(BlackboardAsset, Blackboard);
	RunBehaviorTree(BehaviorTressAsset);
}
