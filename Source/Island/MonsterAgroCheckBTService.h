// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "MonsterAgroCheckBTService.generated.h"

/**
 * 
 */

UCLASS()
class ISLAND_API UMonsterAgroCheckBTService : public UBTService
{
	GENERATED_BODY()
	
protected:

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
private:
	UBehaviorTree* ThisTree;
	class AMonsterAIController* ThisController;
	class AIslandMonsterCharacter* ThisAICharacter;

	UPROPERTY(EditAnywhere)
		float AgroRange = 1500.f;

	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<AActor>> IgnoredActorClass;

	UPROPERTY(EditAnywhere)
		TArray<TEnumAsByte<ECollisionChannel>> OtherTargetObjectType;
};
