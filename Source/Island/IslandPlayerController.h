// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Engine/Texture2D.h"
#include "IslandPlayerController.generated.h"

/**
 * 
 */
class UInGameUIWidget;

UCLASS()
class ISLAND_API AIslandPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AIslandPlayerController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		TSubclassOf<UInGameUIWidget> CharacterHUDWidgetClass;

	UPROPERTY()
		UInGameUIWidget* CharacterHUD;

public:
	UFUNCTION()
		void RefreshHUDHPBar();
	UFUNCTION()
		void SetHUDHPBar(float CurrentHealth);
	UFUNCTION()
		void RefreshHUDInventory();
	UFUNCTION()
		void SetInventorySlot(int32 NumSlot, UTexture2D* ItemImage);
};
