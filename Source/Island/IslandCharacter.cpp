// Fill out your copyright notice in the Description page of Project Settings.

#include "IslandCharacter.h"
#include "IslandPlayerCharacter.h"

// Sets default values
AIslandCharacter::AIslandCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	//무브먼트 oriented rotation to movement 설정
	GetCharacterMovement()->bOrientRotationToMovement = true;
	bUseControllerRotationYaw = false;

	//무기 메쉬 생성
	LeftWeaponComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("LeftWeaponComponent"));
	LeftWeaponComponent->SetupAttachment(GetMesh(), "Hand_L");


	//공격 박스 생성
	LeftAttackBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("LeftAttackBoxComponent"));
	LeftAttackBoxComponent->SetupAttachment(LeftWeaponComponent);
	LeftAttackBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	LeftAttackBoxComponent->SetCollisionObjectType(ECC_GameTraceChannel3);

	RightWeaponComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("RightWeaponComponent"));
	RightWeaponComponent->SetupAttachment(GetMesh(), "Hand_R");


	//공격 박스 생성
	RightAttackBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("RightAttackBoxComponent"));
	RightAttackBoxComponent->SetupAttachment(RightWeaponComponent);
	RightAttackBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RightAttackBoxComponent->SetCollisionObjectType(ECC_GameTraceChannel3);

	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;

	LeftDefaultAtkRange = LeftAttackBoxComponent->GetComponentScale();
	RightDefaultAtkRange = RightAttackBoxComponent->GetComponentScale();
}

// Called when the game starts or when spawned
void AIslandCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (LeftAttackBoxComponent)
		LeftAttackBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AIslandCharacter::CheckAttackRange);

	if (RightAttackBoxComponent)
		RightAttackBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AIslandCharacter::CheckAttackRange);


}

// Called every frame
void AIslandCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


// Called to bind functionality to input

void AIslandCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float AIslandCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

void AIslandCharacter::MyTakeDamage_Implementation(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

bool AIslandCharacter::ServerAttack_Validate()
{
	return true;
}

void AIslandCharacter::ServerAttack_Implementation()
{
	bIsClickAttack = true;
	LeftAttackBoxComponent->SetRelativeScale3D(LeftDefaultAtkRange);
	RightAttackBoxComponent->SetRelativeScale3D(RightDefaultAtkRange);
}

void AIslandCharacter::SetIsClickAttackFalse()
{
	bIsClickAttack = false;
}


void AIslandCharacter::CheckAttackRange(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (Role == ROLE_Authority && OtherActor != this)
	{

		AttackedActor.Add(OtherActor);

		for (int i = 0; i < AttackedActor.Num(); i++)
		{
			if (AttackedActor[i] == OtherActor)
			{
				return;
			}
		}

		for (int i = 0; i < AttackingClass.Num(); i++)
		{
			if (AttackingClass[i]->IsChildOf(AIslandCharacter::StaticClass()))
			{
				if (OtherActor->GetClass()->IsChildOf(AttackingClass[i]) && OtherComp == Cast<AIslandCharacter>(OtherActor)->GetCapsuleComponent())
				{

					AIslandCharacter* AttackedCharacter = Cast<AIslandCharacter>(OtherActor);
					FDamageEvent ThisEvent(UDamageType::StaticClass());
					AttackLocation = OverlapComp->GetComponentLocation();

					AttackedCharacter->TakeDamage(CurrentDamage * FMath::RandRange(1.f - RandomDamageRange, 1.f + RandomDamageRange), ThisEvent, this->GetController(), this);


					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attack Character");
				}
			}
			else
			{
				if (OtherActor->GetClass()->IsChildOf(AttackingClass[i]))
				{
					AActor* AttackedActor = Cast<AActor>(OtherActor);
					FDamageEvent ThisEvent(UDamageType::StaticClass());
					AttackLocation = OverlapComp->GetComponentLocation();
					AttackedActor->TakeDamage(CurrentDamage * FMath::RandRange(1.f - RandomDamageRange, 1.f + RandomDamageRange), ThisEvent, this->GetController(), this);

					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Attack Actor");
				}
			}
		}
	}
}



bool AIslandCharacter::GetIsClickAttack() const
{
	return bIsClickAttack;
}

ECharacterState AIslandCharacter::GetCurrentState() const
{
	return CurrentState;
}

void AIslandCharacter::SetCurrentState(ECharacterState State)
{
	CurrentState = State;
}

void AIslandCharacter::WeaponCollisionOn()
{
	if (bIsLeftHand)
		LeftAttackBoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	if (bIsRightHand)
		RightAttackBoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

void AIslandCharacter::WeaponCollisionOff()
{
	if (bIsLeftHand)
		LeftAttackBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	if (bIsRightHand)
		RightAttackBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	AttackedActor.Empty();
}

FVector AIslandCharacter::GetWeaponComponentLocation() const
{
	if (bIsLeftHand&&bIsRightHand)
		return (LeftAttackBoxComponent->GetComponentLocation() + RightAttackBoxComponent->GetComponentLocation()) / 2;

	else if (bIsLeftHand)
		return LeftAttackBoxComponent->GetComponentLocation();

	else
		return RightAttackBoxComponent->GetComponentLocation();

}

FVector AIslandCharacter::GetAttackedLocation() const
{
	return AttackLocation;
}

float AIslandCharacter::GetWalkSpeed() const
{
	return WalkSpeed;
}

float AIslandCharacter::GetRunSpeed() const
{
	return RunSpeed;
}