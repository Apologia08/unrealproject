// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PickupItem.h"
#include "Engine/Texture2D.h"
#include "InGameUIWidget.generated.h"

/**
 * 
 */
UCLASS()
class ISLAND_API UInGameUIWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void RefreshHPBar();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void SetHPBar(float CurrentHealth);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void RefreshInventory();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void SetInventorySlot(int32 NumSlot, UTexture2D* ItemImage);
	
};
