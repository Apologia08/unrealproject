// Fill out your copyright notice in the Description page of Project Settings.

#include "IslandPlayerController.h"
#include "InGameUIWidget.h"


AIslandPlayerController::AIslandPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
}


void AIslandPlayerController::BeginPlay()
{
	if (CharacterHUDWidgetClass)
	{
		//CharacterHUD = CreateWidget<UInGameUIWidget>(this, CharacterHUDWidgetClass);

		if (CharacterHUD)
		{
			CharacterHUD->AddToViewport();
			RefreshHUDHPBar();
			RefreshHUDInventory();
		}
	}
}

void AIslandPlayerController::RefreshHUDHPBar()
{
	if(CharacterHUD)
		CharacterHUD->RefreshHPBar();
}

void AIslandPlayerController::SetHUDHPBar(float CurrentHealth)
{
	if (CharacterHUD)
		CharacterHUD->SetHPBar(CurrentHealth);
}

void AIslandPlayerController::RefreshHUDInventory()
{
	if(CharacterHUD)
		CharacterHUD->RefreshInventory();
}

void AIslandPlayerController::SetInventorySlot(int32 NumSlot, UTexture2D* ItemImage)
{
	if (CharacterHUD)
		CharacterHUD->SetInventorySlot(NumSlot, ItemImage);
}