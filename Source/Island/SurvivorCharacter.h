// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IslandPlayerCharacter.h"
#include "Components/SphereComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/Texture2D.h"
#include "SurvivorCharacter.generated.h"


/**
 *
 */
USTRUCT(BlueprintType)
struct FRandomSpawnClass
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<ASpawnItem>> SpawnItemClass;
};

USTRUCT(BlueprintType)
struct FSpawnList
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int SpawnLevel = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float PlusHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FRandomSpawnClass RandomSpawnItemList;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FKey Key;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float CoolDownTime;

	UPROPERTY(BlueprintReadWrite)
		FTimerHandle TimerHandle;
};

USTRUCT(BlueprintType)
struct FBuffInfo
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(BlueprintReadOnly)
		float BuffRate = 0.f;

	UPROPERTY(BlueprintReadOnly)
		FTimerHandle TimerHandle;

	UPROPERTY(EditAnywhere)
		UParticleSystemComponent* BuffEffect;
};



UCLASS()
class ISLAND_API ASurvivorCharacter : public AIslandPlayerCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASurvivorCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


private:
	FVector CurrentMouseCoord;

protected:

	FSpawnList TargetSpawnItemList;
	int8 CurrentSpawnItemIndex;
	ASpawnItem* MouseSpawnItem;
	class ASurvivorPlayerState* SurvivorPlayerState;
	//TArray<FTimerHandle> BuffTimers;
	//TArray<float> BuffRates;
	//TArray<UParticleSystemComponent*> BuffEffects;

	FTimerHandle ComboTimer;
	FTimerHandle AnimationTimer;
	bool bIsRunning = false;

	virtual bool ServerPickup_Validate(APickupItem* PickupItem) override;
	virtual void ServerPickup_Implementation(APickupItem* PickupItem) override;
	virtual void ServerAttack_Implementation() override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void ClickSkill(FKey Key) override;
	virtual void CheckAttackRange(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult) override;



	UPROPERTY(BlueprintReadOnly)
		TArray<FBuffInfo> BuffInfos;

	UPROPERTY(BlueprintReadOnly)
		bool bIsGenerateMode = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnItem")
		TArray<FSpawnList> SpawnItemList;

	UPROPERTY(EditAnywhere, Category = "SpawnItem")
		float SpawnRange = 200.f;

	UPROPERTY(VisibleAnywhere)
		USphereComponent* PickupMaterialRange;

	UPROPERTY(BlueprintReadWrite, Category = "Combo")
		int32 ComboCount = 0;

	UPROPERTY(EditAnywhere, Category = "Combo")
		float ComboResetTime = 5.f;

protected:

	void Run();
	void Walk();
	void ResetAnimationTimer();

	//bIsGenerateMode값을 변경

	void SetGenerateMode(FKey Key);
	void OffGenerateMode();
	void SpawnItem();
	void ChooseCurrentSpawnIndex(FKey Key);
	void ResetSpawnTimer();

	//mouse pointer에 mesh 생성

	ASpawnItem* GenerateMeshAtMouse(TSubclassOf<ASpawnItem> ChosenItem, FVector MouseCoord);
	FVector WorldToCoord(FVector WorldLocation, int32 CoordRange);

	//UFUNCTION()
	//	FVector MoveSpawnItem(AActor* MovedActor);

	bool CheckEnoughNumMaterial(ASpawnItem* TargetItem);
	void SubNumMaterial(ASpawnItem* TargetItem);

	//Material 줍기
	void Pickup(int8 InventorySlotNumber, APickupItem* PickupItem);
	void UseItem(FKey Key);

	void ResetBuff();
	void ResetCombo();

	UFUNCTION()
		void AutoPickup(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerSpawnItem(TSubclassOf<ASpawnItem> ChosenItem, FVector MouseCoord);

	UFUNCTION(NetMulticast, Reliable)
		void MulticastSpawnItem(TSubclassOf<ASpawnItem> ChosenItem, FVector MouseCoord);

	UFUNCTION(NetMulticast, Reliable)
		void MulticastAttach(APickupItem* PickupItem);

	UFUNCTION(Client, Reliable)
		void ClientSetInventorySlot(int32 NumSlot, UTexture2D* ItemImage);

	UFUNCTION(BlueprintCallable)
		float GetBuffAtkSpeed();

public:

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void AddExp(float Exp);

};
