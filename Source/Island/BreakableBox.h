// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BreakableActor.h"
#include "BreakableBox.generated.h"

/**
 * 
 */
class AItem;

USTRUCT()
struct FDropItem
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnyWhere)
		TSubclassOf<AItem> Item;

	UPROPERTY(EditAnyWhere)
		int8 NumDrop;

	UPROPERTY(EditAnyWhere)
		float DropPossibilty;
};

USTRUCT()
struct FDropGroup
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnyWhere)
		TArray<FDropItem> DropItemList;
};

UCLASS()
class ISLAND_API ABreakableBox : public ABreakableActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABreakableBox();

protected:

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditAnywhere, Category = "SpawnItemList")
		TArray<FDropGroup> DropGroupList;

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerSpawnItem();

	UFUNCTION()
		void SpawnFromDropItemList(FDropGroup DropGroup);
};
