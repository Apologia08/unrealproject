// Fill out your copyright notice in the Description page of Project Settings.

#include "MonsterAgroCheckBTService.h"
#include "Engine/World.h"
#include "MonsterAIController.h"
#include "IslandMonsterCharacter.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"


void UMonsterAgroCheckBTService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	if (ThisTree == nullptr && ThisController == nullptr && ThisAICharacter == nullptr)
	{
		ThisTree = OwnerComp.GetCurrentTree();
		ThisController = Cast<AMonsterAIController>(OwnerComp.GetAIOwner());
		ThisAICharacter = Cast<AIslandMonsterCharacter>(OwnerComp.GetAIOwner()->GetPawn());

		if (ThisTree == nullptr && ThisController == nullptr && ThisAICharacter == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("Invalid AI"));
			return;
		}
	}

	FCollisionQueryParams SphereSweepParams(FName(TEXT("AgroCheckSweep")), true, ThisAICharacter);

	for (int8 i = 0; i < IgnoredActorClass.Num(); i++)
	{
		TArray<AActor*> OutActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), IgnoredActorClass[i], OutActors);
		SphereSweepParams.AddIgnoredActors(OutActors);
	}

	FCollisionObjectQueryParams ObjectQuery;

	for (int8 i = 0; i < OtherTargetObjectType.Num(); i++)
	{
		ObjectQuery.AddObjectTypesToQuery(OtherTargetObjectType[i]);
	}

	FHitResult HitOut(ForceInit);

	DrawDebugSphere(GetWorld(), ThisAICharacter->GetActorLocation(), AgroRange, 12, FColor::Red, false, 4.0f);

	bool bResult = GetWorld()->SweepSingleByObjectType(HitOut, ThisAICharacter->GetActorLocation(), ThisAICharacter->GetActorLocation() + FVector(0.f, 0.f, 10.f), FQuat(), ObjectQuery, FCollisionShape::MakeSphere(AgroRange), SphereSweepParams);

	if (bResult)
	{
		ThisController->GetBlackboardComponent()->SetValueAsObject(TEXT("TargetToFollow"), HitOut.GetActor());
		ThisController->GetBlackboardComponent()->SetValueAsVector(TEXT("TargetLocation"), HitOut.GetActor()->GetActorLocation());
	}
	else
	{
		ThisController->GetBlackboardComponent()->SetValueAsObject(TEXT("TargetToFollow"), UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		ThisController->GetBlackboardComponent()->SetValueAsVector(TEXT("TargetLocation"), UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetActorLocation());
	}
}


