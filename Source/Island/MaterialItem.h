// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "AutoPickupItem.h"
#include "Components/CapsuleComponent.h"
#include "MaterialItem.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class EKindOfItemMaterial : uint8
{
	WOOD,
	STEEL,
	CORPSE,
	MATERIALCOUNT UMETA(Hidden)
};

UCLASS()
class ISLAND_API AMaterialItem : public AAutoPickupItem
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMaterialItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:

	virtual void Pickup(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult) override;

	UPROPERTY(EditAnywhere)
		EKindOfItemMaterial ItemMaterial;
	
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerAddMaterialToState(ASurvivorCharacter* TargetSurvivor);

	UFUNCTION(NetMulticast, Reliable)
		void MulticastDestroy();

};